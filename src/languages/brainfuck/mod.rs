use std::io::{self, BufRead};
use std::{
    fs::File,
    io::{Read, Write},
    path::PathBuf,
};
use structopt::StructOpt;
use thiserror::Error;

#[derive(Debug, StructOpt)]
pub struct Options {
    source: PathBuf,
}

#[derive(Debug, Error)]
pub enum RunError {
    #[error("no such file")]
    NoFile(#[from] io::Error),
}

#[derive(Debug)]
pub struct State<I, O>
where
    I: Read,
    O: Write,
{
    pointer: usize,
    program_counter: usize,
    memory: [u8; 30_000],
    source: Vec<u8>,
    input: I,
    output: O,
}

#[derive(Debug)]
enum Action {
    MoveRight,
    MoveLeft,
    Increment,
    Decrement,
    Output,
    Input,
    OpenLoop,
    CloseLoop,
    Invalid,
}

struct OutOfBounds {}

impl From<u8> for Action {
    fn from(byte: u8) -> Self {
        match byte {
            b'>' => Self::MoveRight,
            b'<' => Self::MoveLeft,
            b'+' => Self::Increment,
            b'-' => Self::Decrement,
            b'.' => Self::Output,
            b',' => Self::Input,
            b'[' => Self::OpenLoop,
            b']' => Self::CloseLoop,
            _ => Self::Invalid,
        }
    }
}

impl<I: Read, O: Write> State<I, O> {
    fn init<S>(source: &mut S, input: I, output: O) -> io::Result<State<I, O>>
    where
        S: Read,
    {
        let mut buffer = Vec::new();
        source.read_to_end(&mut buffer)?;
        Ok(Self {
            pointer: 0,
            program_counter: 0,
            memory: [0; 30_000],
            source: buffer,
            input,
            output,
        })
    }

    fn run(&mut self) {
        while let Ok(action) = self.read() {
            match action {
                Action::MoveRight => {
                    self.move_right();
                }
                Action::MoveLeft => {
                    self.move_left();
                }
                Action::Increment => {
                    self.increment();
                }
                Action::Decrement => {
                    self.decrement();
                }
                Action::Input => {
                    self.input();
                }
                Action::Output => {
                    self.output();
                }
                Action::OpenLoop => {
                    self.open_loop();
                }
                Action::CloseLoop => {
                    self.close_loop();
                }
                _ => {}
            }

            // let mem: Vec<_> = self.memory.chunks(10).collect();
            // println!("{:?}", mem[0]);
            self.program_counter = self.program_counter + 1;
        }
    }

    fn move_right(&mut self) {
        self.pointer = (self.pointer + 1) % 30_000;
    }

    fn move_left(&mut self) {
        if self.pointer == 0 {
            self.pointer = 29_999;
        } else {
            self.pointer = self.pointer - 1;
        }
    }

    fn increment(&mut self) {
        let value = self.memory[self.pointer].wrapping_add(1);
        self.memory[self.pointer] = value;
    }

    fn decrement(&mut self) {
        let value = self.memory[self.pointer].wrapping_sub(1);
        self.memory[self.pointer] = value;
    }

    fn input(&mut self) {
        let mut buffer = [0; 1];
        if let Ok(()) = self.input.read_exact(&mut buffer) {
            self.memory[self.pointer] = buffer[0];
        } else {
            eprintln!("could not read from input");
        }
    }

    fn output(&mut self) {
        if let Err(_) = self
            .output
            .write(&self.memory[self.pointer..self.pointer + 1])
        {
            eprint!("could not write to output");
        }
    }

    fn open_loop(&mut self) {
        let value = self.memory[self.pointer];
        if value == 0 {
            // Look ahead in the source
            // until a matching closing bracket
            // is found
            let mut position = self.program_counter;
            let mut level: u32 = 0;
            loop {
                if self.source[position] == b'[' {
                    level = level + 1;
                } else if self.source[position] == b']' {
                    level = level - 1;
                }

                if self.source[position] == b']' && level == 0 {
                    break;
                }

                position = position + 1;
            }
            self.program_counter = position;
        } else {
            self.program_counter = self.program_counter + 1;
        }
    }

    fn close_loop(&mut self) {
        let value = self.memory[self.pointer];
        if value != 0 {
            // Look back in the source
            // until a matching opening
            // bracket is found
            let mut position = self.program_counter;
            let mut level: u32 = 0;
            loop {
                if self.source[position] == b']' {
                    level = level + 1;
                } else if self.source[position] == b'[' {
                    level = level - 1;
                }

                if self.source[position] == b'[' && level == 0 {
                    break;
                }

                position = position - 1;
            }

            self.program_counter = position;
        } else {
            self.program_counter = self.program_counter + 1;
        }
    }

    fn read(&self) -> Result<Action, OutOfBounds> {
        if let Some(&char) = self.source.get(self.program_counter) {
            Ok(Action::from(char))
        } else {
            Err(OutOfBounds {})
        }
    }
}

// pub fn interpreter<I,O>(input: I, output: O, state: &mut State) where I: Read, O: Write {
//     while let Ok(action) = state.read() {
//         match action {
//             Action::MoveRight => { state.move_right(); },
//             Action::MoveLeft => { state.move_left(); },
//             Action::Increment => { state.increment(); },
//             Action::Decrement => { state.decrement(); },
//             _ => { },
//         }

//         let mem: Vec<_> = state.memory.chunks(10).collect();
//         println!("{:?}", mem[0]);
//         state.advance();
//     }
// }

pub fn run(options: &Options) -> Result<(), RunError> {
    let stdin = io::stdin();
    let stdout = io::stdout();

    let mut source = File::open(&options.source)?;
    let input = stdin.lock();
    let output = stdout.lock();
    let mut state = State::init(&mut source, input, output)?;

    state.run();
    // interpreter(input, output, &mut state);

    Ok(())
}

pub fn main(options: &Options) {
    run(options);
}
