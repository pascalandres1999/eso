mod languages;

use structopt::StructOpt;
use std::error::Error;

#[derive(Debug, StructOpt)]
#[structopt(name = "eso", about = "Compiler for various esoteric languages")]
enum CommandOptions {
    Brainfuck(languages::brainfuck::Options),
}

fn main() {
    let options = CommandOptions::from_args();

    match options {
        CommandOptions::Brainfuck(options) => { languages::brainfuck::main(&options) },
    }
}
